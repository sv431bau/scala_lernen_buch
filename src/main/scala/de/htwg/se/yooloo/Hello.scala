package de.htwg.se.yooloo

import de.htwg.se.yooloo.model.Student

object Hello {
  def main(args: Array[String]): Unit = {
    val student = Student("Sven Bauersfeld and Viktoria Schreiner")
    println("Hello, " + student.name)
  }
}
