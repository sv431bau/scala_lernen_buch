package de.htwg.se.yooloo.model

case class Student(name: String) {
  def f(x: Int): Int = x + 1
}

